require.config({

	deps:['app/white.index'],
	
	paths:{

		"jquery":"lib/jquery/jquery.min",
		"utils.eventListener":"utils/utils.eventListener",
		"utils.requestAnimation":"utils/utils.requestAnimation",
		"white.lightbox":"app/white.lightbox"

	}


})