define([

	"white.lightbox"

],function(LightBox){

	var lightbox = new LightBox('js/data.json');
	var reset = document.getElementById('reset');

	reset.onclick = function(e){
		lightbox.reset();
		return false;
	}

})