define([

	"jquery",
	"utils.eventListener",
	"utils.requestAnimation"

],function(jquery, EventListener, RequestAnimation){

	function LightBox(url){
		//
		this.url = url
		//
		this.container = document.getElementById('lightBox');
		this.barLoad = document.getElementById('loading');
		this.barDone = document.getElementById('complete');
		this.output = document.getElementById('output');
		this.close = document.getElementById('close');
		//
		this.dead = true;
		//
		this.barWidth = this.barLoad.offsetWidth;
		//
		this.addEventListeners();
		//
		this.init();

	}

	LightBox.prototype = new EventListener();

	LightBox.prototype.constructor = LightBox;

	LightBox.prototype.init = function(){

		var self = this;

		if(self.dead) self.dead = false;
		
		self.container.style.display = 'table';
		self.barLoad.style.left = -self.barLoad.offsetWidth + 'px';

		$.getJSON(self.url, function(res) {self.load(res.data.lightbox);});

	}

	LightBox.prototype.load = function(data){

		var self = this;
		var duration = (data.duration/1000)*60;
		var count = 0;

		var loop = function(){

			if((count/duration)*100 < 100)
			{
				count++;
				self.update((count/duration)*100);
				(!self.dead) ? RequestAnimation(loop) : count = 0
			}
			else
			{
				self.update(100);
				(!self.dead) ? self.complete() : count = 0;
			}
		}

		loop();

	}

	LightBox.prototype.update = function(percent){
		this.output.innerHTML = 'Progress ' + Math.ceil(percent).toString() + '%';
		this.barLoad.style.left = -(this.barWidth-((this.barWidth/100)*percent)) + 'px';
	}

	LightBox.prototype.complete = function(){
		
		var self = this;
		self.barLoad.style.display = 'none';
		self.barDone.style.display = 'block';
		self.output.innerHTML = 'This task is 100% complete<span></span>';

	}

	LightBox.prototype.reset = function(e){
		this.init();
	}

	LightBox.prototype.kill = function(){
		this.dead = true;
		this.barLoad.style.display = 'block';
		this.barDone.style.display = 'none';
		this.output.innerHTML = 'Progress 0%';
	}

	LightBox.prototype.addEventListeners = function(){

		var self = this;

		this.close.onclick = function(e){
			self.kill();
			self.container.style.display = 'none';
			return false;
		}
		
	}	

	return LightBox;

})