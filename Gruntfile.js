var fs = require('fs');

module.exports = function(grunt) {
  
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-exec');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    cssmin: {
      combine: {
        files: {
          'dist/css/<%= pkg.name %>-style.css': 'css/*.css'
        }
      },
      minify: {
        expand: true,
        src: ['dist/css/<%= pkg.name %>-style.css', 'dist/css/!<%= pkg.name %>-style.min.css'],
        dest: '',
        ext: '.min.css'
      }
    },
    requirejs: {
      compile: {
        options: {
          almond: false,
          separateCSS: true,
          mainConfigFile: "js/require.config.js",
          out: "dist/js/<%= pkg.name %>.js",
          name: "require.config",
          wrap: false,
          optimize: "none"
        }
      }
    },
    uglify: {
      options: {
        report: 'gzip',
        preserveComments: 'false',
        mangle: {
          except: []
        }
      },
      dist: {
        src: ['dist/js/<%= pkg.name %>.js'],
        dest: 'dist/js/<%= pkg.name %>.min.js'
      }
    },

    exec: {
      clear_css: {
        command: 'rm dist/css/<%= pkg.name %>-style.css'
      },
      clear_js: {
        command: 'rm dist/js/<%= pkg.name %>.js'
      },
      copy_images_to_dist: {
        command: 'rm -r dist/images && cp -r images dist/'
      },
      copy_json: {
        command: 'cp -r js/data.json dist/js'
      }
    }

  });

  grunt.task.registerTask('index', 'Copy the index file and prep for distribution', function(){

    var done = this.async();
    var indexData = fs.readFileSync('index.html').toString('utf8');
    var pkg = grunt.file.readJSON('package.json');

    indexData = indexData.split(/\r?\n/).map( function(line){

      if(/stylesheet/i.test(line) && /white.index/i.test(line)) return '    <link rel="stylesheet" type="text/css" href="css/'+pkg.name+'-style.min.css">';
      if(/data-main/i.test(line) && /require/i.test(line)) return '    <script type="text/javascript" data-main="js/'+pkg.name+'.min.js" src="js/require.js"></script>';
      return line;

    }).join('\n');
    //
    if(!fs.existsSync('dist')) fs.mkdir('dist');
    if(!fs.existsSync('dist/js')) fs.mkdir('dist/js');
    if(!fs.existsSync('dist/images')) fs.mkdir('dist/images');
    if(!fs.existsSync('dist/css')) fs.mkdir('dist/css');
    //
    fs.writeFileSync('dist/index.html', indexData);
    //
    console.log('File dist/index.html successfully copied, reformatted and published.');
    //
    done();

  });

  // Default task.
  grunt.registerTask('default', ['index','cssmin','requirejs','uglify','exec:clear_css','exec:clear_js','exec:copy_images_to_dist','exec:copy_json']);
  
};